public class hash {
    int M;
    Node[] list;

    public hash() { // default
        this.M = 5;
        list = new Node[M];
    }

    public hash(int M) { // constructor for input number
        this.M = M;
        list = new Node[M];
    }

    class Node {
        int key;
        String val;
        Node next;

        Node(int key1, String val1) {
            key = key1;
            val = val1;
        }
    }

    private int hash(int key) { // find index of array
        int h = key % M;
        return h;
    }

    // get value from input key
    public String get(int key) {
        // always set key that already use hash()
        int h = hash(key);
        for (Node i = list[h]; i != null; i = i.next) {
            if (key == i.key) {
                return i.val;
            }
        }
        return null;
    }

    public void put(int key, String val) {
        int h = hash(key);
        Node head = list[h];

        while (head != null) {
            if ((head.key) == key) {
                head.val = val;
                return;
            }
            head = head.next;
        }
        head = list[h];
        Node node = new Node(key, val);
        node.next = head;
        list[h] = node;
    }

    public void remove(int key) {
        int h = hash(key);
        Node head = list[h];
        Node prev = null;

        while (head != null) {
            if (head.key == key) {
                break;
            }
            prev = head;
            head = head.next;
        }
        if (head == null) {
            return;
        }

        if (prev != null) {
            prev.next = head.next;
        } else {
            list[h] = head.next;
        }
    }

    public void getAll() {
        for (int i = 0; i < M; i++) {
            for (Node j = list[i]; j != null; j = j.next) {
                System.out.println("Index: "+i + " Key: " + j.key + " " +" Value: "+ j.val);
            }
        }
    }
}
